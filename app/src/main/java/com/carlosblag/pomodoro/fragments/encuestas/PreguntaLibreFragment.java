package com.carlosblag.pomodoro.fragments.encuestas;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.carlosblag.pomodoro.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PreguntaLibreFragment extends Fragment {

    private static final String CONTADOR = "contador";
    @Bind(R.id.enunciado)
    TextView enunciado;
    @Bind(R.id.count)
    TextView count;
    @Bind(R.id.respuesta)
    EditText respuesta;

    public PreguntaLibreFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PreguntaLibreFragment newInstance(Integer idEncuesta) {
        PreguntaLibreFragment fragment = new PreguntaLibreFragment();
        Bundle args = new Bundle();

        args.putInt("idEncuesta", idEncuesta);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pregunta_libre, container, false);
        ButterKnife.bind(this, rootView);


        count.setText(getArguments().getString(CONTADOR));
        enunciado.setText("Esta es mi id de encuesta= " + getArguments().getString("idEncuesta"));
        respuesta.setHint("Pon aquí tu respuesta");
        respuesta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //pon un contador
            }
        });
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
