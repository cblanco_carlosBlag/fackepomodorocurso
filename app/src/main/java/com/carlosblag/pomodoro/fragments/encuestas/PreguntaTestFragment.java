package com.carlosblag.pomodoro.fragments.encuestas;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carlosblag.pomodoro.R;
import com.carlosblag.pomodoro.RespuestasListAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PreguntaTestFragment extends Fragment {


    private static final String CONTADOR = "contador";
    @Bind(R.id.enunciado)
    TextView enunciado;
    @Bind(R.id.count)
    TextView count;
    @Bind(R.id.list_respuestas)
    RecyclerView rv;


    private boolean fragmentUpdated;
    private RespuestasListAdapter adapter;

    public PreguntaTestFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PreguntaTestFragment newInstance(Integer idEncuesta) {
        PreguntaTestFragment fragment = new PreguntaTestFragment();

        Bundle args = new Bundle();
        args.putString(CONTADOR, "33");

        args.putInt("idEncuesta", idEncuesta);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pregunta_test, container, false);
        ButterKnife.bind(this, rootView);
        fragmentUpdated = false;

        count.setText(getArguments().getString(CONTADOR));
        enunciado.setText("Soy un enunciado de la encusta " + getArguments().get("idEncuesta") + " tu que eres...");
        //Necesitamos para el recycled un manager
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);
        ArrayList<String> preguntas = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            preguntas.add("Hola soy la pregunta " + i);
        }
        adapter = new RespuestasListAdapter(preguntas, getContext());
        rv.setAdapter(adapter);
        rv.setItemAnimator(new DefaultItemAnimator());
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


}
