package com.carlosblag.pomodoro.fragments.encuestas;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.carlosblag.pomodoro.R;
import com.carlosblag.pomodoro.RespuestasListAdapter;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;


public class PreguntaTestInfladaFragment extends Fragment {


    private static final String CONTADOR = "contador";
    @Bind(R.id.enunciado)
    TextView enunciado;
    @Bind(R.id.count)
    TextView count;
    @Bind(R.id.list_respuestas)
    LinearLayout listRespuestas;


    private boolean fragmentUpdated;
    private RespuestasListAdapter adapter;

    public PreguntaTestInfladaFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PreguntaTestInfladaFragment newInstance(Integer idEncuesta) {
        PreguntaTestInfladaFragment fragment = new PreguntaTestInfladaFragment();

        Bundle args = new Bundle();
        args.putString(CONTADOR, "33");

        args.putInt("idEncuesta", idEncuesta);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pregunta_inflada_test, container, false);
        ButterKnife.bind(this, rootView);
        fragmentUpdated = false;

        count.setText(getArguments().getString(CONTADOR));
        enunciado.setText("Soy un enunciado de la encusta " + getArguments().get("idEncuesta") + " tu que eres...");

        ArrayList<String> respuestas = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            respuestas.add("Hola soy la pregunta inflada " + i);
        }

        for (String respuesta : respuestas) {
            LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.row_respuesta_inflada_test, container, false);
            Button respuestaTxt = (Button) linearLayout.findViewById(R.id.respuestaInflada);
            respuestaTxt.setText(respuesta);
            respuestaTxt.setTag(respuesta);
            listRespuestas.addView(linearLayout);
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


/*
    @OnClick(R.id.list_respuestas)
    public void onClick() {
        *//*cbla poned que en el onclick se muestre que posición has tocado.*//*
        Toast.makeText(getContext(), "si podéis", Toast.LENGTH_SHORT).show();
    }*/
}
