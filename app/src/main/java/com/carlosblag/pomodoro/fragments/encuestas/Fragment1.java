package com.carlosblag.pomodoro.fragments.encuestas;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.carlosblag.pomodoro.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

;


public class Fragment1 extends Fragment {


    @Bind(R.id.enviar_texto)
    TextView enviarTexto;
    @Bind(R.id.enviar_boton)
    TextView enviarBoton;

    public Fragment1() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static Fragment1 newInstance(Integer idEncuesta) {
        Fragment1 fragment = new Fragment1();

        Bundle args = new Bundle();

        args.putInt("idEncuesta", idEncuesta);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enviar, container, false);
        ButterKnife.bind(this, rootView);

        //cbla recuperar id encuesta para un texto y mostrarlo
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    @OnClick(R.id.enviar_boton)
    public void onClick() {

    }


}
