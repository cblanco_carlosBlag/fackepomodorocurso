package com.carlosblag.pomodoro;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jfer on 29/7/15.
 */
public class RespuestasListAdapter extends RecyclerView.Adapter<RespuestasListAdapter.RespuestaTestItemViewHolder> {

    private final Context context;
    private List<String> items = new ArrayList<>();
    private int position;


    public RespuestasListAdapter(ArrayList<String> preguntas, Context context) {
        this.items = preguntas;
        this.context = context;
    }

    @Override
    public RespuestaTestItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_respuesta_test, viewGroup, false);
        RespuestaTestItemViewHolder pvh = new RespuestaTestItemViewHolder(v);


        return pvh;
    }

    @Override
    public void onBindViewHolder(RespuestaTestItemViewHolder respuestaTestItemViewHolder, int i) {

        final String respuestaTest = items.get(i);
        respuestaTestItemViewHolder.respuesta.setText(respuestaTest);


        respuestaTestItemViewHolder.layoutRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRespuestaSelected(respuestaTest, v);
            }
        });
    }

    private void setRespuestaSelected(String respuestaTest, View v) {
        //cbla poner un toastcon el texto


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    static class RespuestaTestItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.respuesta)
        TextView respuesta;
        @Bind(R.id.layoutRespuesta)
        LinearLayout layoutRespuesta;

        RespuestaTestItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
