package com.carlosblag.pomodoro;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.carlosblag.pomodoro.fragments.encuestas.Fragment1;
import com.carlosblag.pomodoro.fragments.encuestas.HomeConTabs;
import com.carlosblag.pomodoro.fragments.encuestas.PreguntaLibreFragment;
import com.carlosblag.pomodoro.fragments.encuestas.PreguntaTestFragment;
import com.carlosblag.pomodoro.fragments.encuestas.PreguntaTestInfladaFragment;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //lo eliminamos no lo usamos
      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        Fragment fragment = null;

        int id = menuItem.getItemId();

        if (id == R.id.nav_camera) {
            fragment = Fragment1.newInstance(55);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_gallery) {
            fragment = Fragment1.newInstance(58);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_slideshow) {
            fragment = PreguntaLibreFragment.newInstance(56);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_manage) {
            fragment = PreguntaTestFragment.newInstance(22222);
            menuItem.setChecked(true);
        } else if (id == R.id.nav_share) {
            goToNextActivity();
        } else if (id == R.id.nav_send) {
            fragment = PreguntaTestInfladaFragment.newInstance(1234567);
        }
        if (null != fragment) {
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }

        // Set action bar title
        setTitle(menuItem.getTitle());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void goToNextActivity() {
        Intent intent = new Intent(this, HomeConTabs.class);
        intent.putExtra("parametro", 22);
        intent.putExtra("parametroString", "22");
        startActivity(intent);
        montar la buena ya de verdad
    }

    /*cbla este metodo viene del fragment inflado para hacer el onclick*/
    public void tocadoHundido(View v) {

        Toast.makeText(this, "si podéis", Toast.LENGTH_SHORT).show();
    }
}
